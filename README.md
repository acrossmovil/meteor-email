Referencia

https://atmospherejs.com/meteor/email

Git

Command line instructions


Git global setup

git config --global user.name "acrossmovil"
git config --global user.email "acrossmovil@gmail.com"

Create a new repository

git clone git@gitlab.com:acrossmovil/meteor-email.git
cd meteor-email
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder or Git repository

cd existing_folder
git init
git remote add origin git@gitlab.com:acrossmovil/meteor-email.git
git add .
git commit
git push -u origin master
