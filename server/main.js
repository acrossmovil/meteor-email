import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
  smtp = {
      username: 'contacto@across.cl',   // eg: server@gentlenode.com
      password: 'acros_11',   // eg: 3eeP1gtizk5eziohfervU
      server:   'smtp.gmail.com',  // eg: mail.gandi.net
      port: 587
    }

    process.env.MAIL_URL = 'smtp://' + encodeURIComponent(smtp.username) + ':' + encodeURIComponent(smtp.password) + '@' + encodeURIComponent(smtp.server) + ':' + smtp.port;

    //process.env.MAIL_URL = 'smtp://contacto%40across.cl:acros_11@smtp.gmail.com:587';
});


Meteor.methods({
  sendEmail: function (to, from, subject, text) {
    check([to, from, subject, text], [String]);

    // Let other method calls from the same client start running,
    // without waiting for the email sending to complete.
    this.unblock();

    Email.send({
      to: to,
      from: from,
      subject: subject,
      text: text
    });
  }
});
